All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    yeast_reversed_contigs
# contigs (>= 0 bp)         1694                  
# contigs (>= 1000 bp)      233                   
# contigs (>= 5000 bp)      185                   
# contigs (>= 10000 bp)     161                   
# contigs (>= 25000 bp)     125                   
# contigs (>= 50000 bp)     84                    
Total length (>= 0 bp)      11929117              
Total length (>= 1000 bp)   11642932              
Total length (>= 5000 bp)   11507345              
Total length (>= 10000 bp)  11335265              
Total length (>= 25000 bp)  10727508              
Total length (>= 50000 bp)  9219023               
# contigs                   276                   
Largest contig              454054                
Total length                11672311              
GC (%)                      38.11                 
N50                         103457                
N75                         59166                 
L50                         37                    
L75                         76                    
# N's per 100 kbp           8.40                  
