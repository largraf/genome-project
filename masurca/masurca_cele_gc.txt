All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    final.genome.scf
# contigs (>= 0 bp)         15699           
# contigs (>= 1000 bp)      10896           
# contigs (>= 5000 bp)      4931            
# contigs (>= 10000 bp)     2706            
# contigs (>= 25000 bp)     716             
# contigs (>= 50000 bp)     122             
Total length (>= 0 bp)      92851789        
Total length (>= 1000 bp)   90263643        
Total length (>= 5000 bp)   75216659        
Total length (>= 10000 bp)  59349704        
Total length (>= 25000 bp)  28107579        
Total length (>= 50000 bp)  8180806         
# contigs                   13134           
Largest contig              156320          
Total length                91872892        
GC (%)                      35.65           
N50                         15480           
N75                         6812            
L50                         1625            
L75                         3851            
# N's per 100 kbp           0.00            
