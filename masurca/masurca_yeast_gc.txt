\documentclass[12pt,a4paper]{article}
\begin{document}
\begin{table}[ht]
\begin{center}
\caption{All statistics are based on contigs of size $\geq$ 500 bp, unless otherwise noted (e.g., "\# contigs ($\geq$ 0 bp)" and "Total length ($\geq$ 0 bp)" include all contigs).}
\begin{tabular}{|l*{1}{|r}|}
\hline
Assembly & final.genome.scf \\ \hline
\# contigs ($\geq$ 0 bp) & 443 \\ \hline
\# contigs ($\geq$ 1000 bp) & 310 \\ \hline
\# contigs ($\geq$ 5000 bp) & 252 \\ \hline
\# contigs ($\geq$ 10000 bp) & 229 \\ \hline
\# contigs ($\geq$ 25000 bp) & 150 \\ \hline
\# contigs ($\geq$ 50000 bp) & 83 \\ \hline
Total length ($\geq$ 0 bp) & 11533568 \\ \hline
Total length ($\geq$ 1000 bp) & 11467169 \\ \hline
Total length ($\geq$ 5000 bp) & 11333430 \\ \hline
Total length ($\geq$ 10000 bp) & 11166373 \\ \hline
Total length ($\geq$ 25000 bp) & 9832326 \\ \hline
Total length ($\geq$ 50000 bp) & 7438262 \\ \hline
\# contigs & 368 \\ \hline
Largest contig & 184687 \\ \hline
Total length & 11505646 \\ \hline
GC (\%) & 38.19 \\ \hline
N50 & 66612 \\ \hline
N75 & 37109 \\ \hline
L50 & 55 \\ \hline
L75 & 111 \\ \hline
\# N's per 100 kbp & 0.00 \\ \hline
\end{tabular}
\end{center}
\end{table}
\end{document}
