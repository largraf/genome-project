#!/bin/bash

time spades.py -o normal1 -1 ~/yeast_reorder/ERR1938683_1.fastq -2 ~/yeast_reorder/ERR1938683_2.fastq

time spades.py -o normal2 -1 ~/yeast_reorder/ERR1938683_1.fastq -2 ~/yeast_reorder/ERR1938683_2.fastq

time spades.py -o alphabetically -1 ~/yeast_reorder/alpha/yeast_alpha_1.fastq -2 ~/yeast_reorder/alpha/yeast_alpha_2.fastq

time spades.py -o rand1 -1 ~/yeast_reorder/rand1/yeast_rand1_1.fastq -2 ~/yeast_reorder/rand1/yeast_rand1_2.fastq

time spades.py -o rand2 -1 ~/yeast_reorder/rand2/yeast_rand2_1.fastq -2 ~/yeast_reorder/rand2/yeast_rand2_2.fastq

time spades.py -o rand3 -1 ~/yeast_reorder/rand3/yeast_rand3_1.fastq -2 ~/yeast_reorder/rand3/yeast_rand3_2.fastq

time spades.py -o decr_mean_quality -1 ~/yeast_reorder/decr_mean_qual/yeast_decr_mean_quality_1.fastq -2 ~/yeast_reorder/decr_mean_qual/yeast_decr_mean_quality_2.fastq

time spades.py -o incr_mean_quality -1 ~/yeast_reorder/incr_mean_qual/yeast_mean_quality_1.fastq -2 ~/yeast_reorder/incr_mean_qual/yeast_mean_quality_2.fastq

time spades.py -o gc -1 ~/yeast_reorder/gc/yeast_gc_1.fastq -2 ~/yeast_reorder/gc/yeast_gc_2.fastq

time spades.py -o reversed -1 ~/yeast_reorder/yeast_reversed_1.fastq -2 ~/yeast_reorder/yeast_reversed_2.fastq
