#!/bin/bash

time spades.py -o normal_1 -1 ~/c_elegans_reorder/SRR3536210_1.fastq -2 ~/c_elegans_reorder/SRR3536210_2.fastq

time spades.py -o normal_2 -1 ~/c_elegans_reorder/SRR3536210_1.fastq -2 ~/c_elegans_reorder/SRR3536210_2.fastq

time spades.py -o alphabetically -1 ~/c_elegans_reorder/alphabetically/c_elegans_alph_1.fastq -2 ~/c_elegans_reorder/alphabetically/c_elegans_alph_2.fastq

time spades.py -o rand1 -1 ~/c_elegans_reorder/rand1/c_elegans_rand_1.fastq -2 ~/c_elegans_reorder/rand1/c_elegans_rand_2.fastq

time spades.py -o rand2 -1 ~/c_elegans_reorder/rand2/c_elegans_rand_1.fastq -2 ~/c_elegans_reorder/rand2/c_elegans_rand_2.fastq
