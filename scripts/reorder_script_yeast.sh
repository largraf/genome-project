#!/bin/bash

fastq-sort --mean-qual ERR1938683_1.fastq > mean_qual/yeast_mean_quality_1.fastq

fastq-sort --mean-qual ERR1938683_2.fastq > mean_qual/yeast_mean_quality_2.fastq

fastq-sort --seq ERR1938683_1.fastq > alpha/yeast_alpha_1.fastq

fastq-sort --seq ERR1938683_2.fastq > alpha/yeast_alpha_2.fastq

fastq-sort --gc ERR1938683_1.fastq > gc/yeast_gc_1.fastq

fastq-sort --gc ERR1938683_2.fastq > gc/yeast_gc_2.fastq

fastq-sort --random  ERR1938683_1.fastq > rand1/yeast_rand1_1.fastq

fastq-sort --random  ERR1938683_2.fastq > rand1/yeast_rand1_2.fastq

fastq-sort --random  ERR1938683_1.fastq > rand2/yeast_rand2_1.fastq

fastq-sort --random  ERR1938683_2.fastq > rand2/yeast_rand2_2.fastq

fastq-sort --random  ERR1938683_1.fastq > rand3/yeast_rand3_1.fastq

fastq-sort --random  ERR1938683_2.fastq > rand3/yeast_rand3_2.fastq

fastq-sort --reverse  ERR1938683_1.fastq > yeast_reverse_1.fastq

fastq-sort --reverse  ERR1938683_2.fastq > yeast_reverse_2.fastq



