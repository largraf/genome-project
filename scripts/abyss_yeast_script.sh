#!/bin/bash

cd rand1
time abyss-pe name=yeast_rand1  k=96 in='~/yeast_reorder/rand1/yeast_rand1_1.fastq ~/yeast_reorder/rand1/yeast_rand1_2.fastq' 
cd ..

cd rand2
time abyss-pe name=yeast_rand2  k=96 in='~/yeast_reorder/rand2/yeast_rand2_1.fastq ~/yeast_reorder/rand2/yeast_rand2_2.fastq' 
cd ..

cd rand3
time abyss-pe name=yeast_rand3  k=96 in='~/yeast_reorder/rand3/yeast_rand3_1.fastq ~/yeast_reorder/rand3/yeast_rand3_2.fastq' 
cd ..

cd reversed
time abyss-pe name=yeast_reversed  k=96 in='~/yeast_reorder/yeast_reversed_1.fastq ~/yeast_reorder/yeast_reversed_2.fastq'
cd ..

cd incr_mean_quality
time abyss-pe name=yeast_incr_mean_qual  k=96 in='~/yeast_reorder/incr_mean_qual/yeast_mean_quality_1.fastq ~/yeast_reorder/incr_mean_qual/yeast_mean_quality_2.fastq'
cd ..

cd decr_mean_quality
time abyss-pe name=yeast_decr_mean_qual  k=96 in='~/yeast_reorder/decr_mean_qual/yeast_decr_mean_quality_1.fastq ~/yeast_reorder/decr_mean_qual/yeast_decr_mean_quality_2.fastq'
cd ..

cd gc
time abyss-pe name=yeast_gc  k=96 in='~/yeast_reorder/gc/yeast_gc_1.fastq ~/yeast_reorder/gc/yeast_gc_2.fastq'
cd ..

cd alphabetically
time abyss-pe name=yeast_alphabetically  k=96 in='~/yeast_reorder/alpha/yeast_alpha_1.fastq ~/yeast_reorder/alpha/yeast_alpha_2.fastq'
cd ..

cd normal1
time abyss-pe name=yeast_norm1 k=96 in='~/yeast_reorder/ERR1938683_1.fastq ~/yeast_reorder/ERR1938683_2.fastq'
cd ..

cd normal2
time abyss-pe name=yeast_norm2 k=96 in='~/yeast_reorder/ERR1938683_1.fastq ~/yeast_reorder/ERR1938683_2.fastq'
cd ..
