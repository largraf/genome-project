#!/bin/bash

cd alphabetically
time ./assemble.sh
cd ..

cd decr_mean_quality 
time ./assemble.sh
cd ..

cd incr_mean_quality
time ./assemble.sh
cd ..

cd gc
time ./assemble.sh
cd ..

cd rand1
time ./assemble.sh
cd ..

cd rand2
time ./assemble.sh
cd ..

cd rand3
time ./assemble.sh
cd ..

cd reversed 
time ./assemble.sh
cd ..

