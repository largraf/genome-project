#!/bin/bash

cd rand1
time abyss-pe name=c_elegans_rand1  k=96 in='~/c_elegans_reorder/rand1/c_elegans_rand_1.fastq ~/c_elegans_reorder/rand1/c_elegans_rand_2.fastq' 
cd ..

cd rand2
time abyss-pe name=c_elegans_rand2  k=96 in='~/c_elegans_reorder/rand2/c_elegans_rand_1.fastq ~/c_elegans_reorder/rand2/c_elegans_rand_2.fastq' 
cd ..

cd rand3
time abyss-pe name=c_elegans_rand3  k=96 in='~/c_elegans_reorder/rand3/c_elegans_rand_1.fastq ~/c_elegans_reorder/rand3/c_elegans_rand_2.fastq' 
cd ..

cd reversed
time abyss-pe name=c_elegans_reversed  k=96 in='~/c_elegans_reorder/c_elegans_reversed_1.fastq ~/c_elegans_reorder/c_elegans_reversed_2.fastq'
cd ..

cd incr_mean_quality
time abyss-pe name=c_elegans_incr_mean_qual  k=96 in='~/c_elegans_reorder/incr_mean_quality/c_elegans_incr_mean_quality_1.fastq ~/c_elegans_reorder/incr_mean_quality/c_elegans_incr_mean_quality_2.fastq'
cd ..

cd decr_mean_quality
time abyss-pe name=c_elegans_decr_mean_qual  k=96 in='~/c_elegans_reorder/decr_mean_quality/c_elegans_incr_mean_quality_rev_1.fastq ~/c_elegans_reorder/decr_mean_quality/c_elegans_incr_mean_quality_rev_2.fastq'
cd ..

cd gc
time abyss-pe name=c_elegans_gc  k=96 in='~/c_elegans_reorder/incr_gc/c_elegans_incr_gc_1.fastq ~/c_elegans_reorder/incr_gc/c_elegans_incr_gc_2.fastq'
cd ..

cd alphabetically
time abyss-pe name=c_elegans_alphabetically  k=96 in='~/c_elegans_reorder/alphabetically/c_elegans_alph_1.fastq ~/c_elegans_reorder/alphabetically/c_elegans_alph_2.fastq'
cd ..

cd normal1
time abyss-pe name=c_elegands_norm1 k=96 in='~/c_elegans_reorder/SRR3536210_1.fastq ~/c_elegans_reorder/SRR3536210_2.fastq'
cd ..

cd normal2
time abyss-pe name=c_elegands_norm2 k=96 in='~/c_elegans_reorder/SRR3536210_1.fastq ~/c_elegans_reorder/SRR3536210_2.fastq'
cd ..
