#!/bin/bash

time spades.py -o rand3 -1 ~/c_elegans_reorder/rand3/c_elegans_rand_1.fastq -2 ~/c_elegans_reorder/rand3/c_elegans_rand_2.fastq

time spades.py -o decr_mean_quality -1 ~/c_elegans_reorder/decr_mean_quality/c_elegans_incr_mean_quality_rev_1.fastq -2 ~/c_elegans_reorder/decr_mean_quality/c_elegans_incr_mean_quality_rev_2.fastq

time spades.py -o incr_mean_quality -1 ~/c_elegans_reorder/incr_mean_quality/c_elegans_incr_mean_quality_1.fastq -2 ~/c_elegans_reorder/incr_mean_quality/c_elegans_incr_mean_quality_2.fastq

time spades.py -o gc -1 ~/c_elegans_reorder/incr_gc/c_elegans_incr_gc_1.fastq -2 ~/c_elegans_reorder/incr_gc/c_elegans_incr_gc_2.fastq

time spades.py -o reversed -1 ~/c_elegans_reorder/c_elegans_reversed_1.fastq -2 ~/c_elegans_reorder/c_elegans_reversed_2.fastq
