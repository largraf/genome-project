All statistics are based on contigs of size >= 500 bp, unless otherwise noted (e.g., "# contigs (>= 0 bp)" and "Total length (>= 0 bp)" include all contigs).

Assembly                    contigs  
# contigs (>= 0 bp)         91063    
# contigs (>= 1000 bp)      6625     
# contigs (>= 5000 bp)      3940     
# contigs (>= 10000 bp)     2675     
# contigs (>= 25000 bp)     1130     
# contigs (>= 50000 bp)     357      
Total length (>= 0 bp)      104783695
Total length (>= 1000 bp)   97101114 
Total length (>= 5000 bp)   90178192 
Total length (>= 10000 bp)  81099735 
Total length (>= 25000 bp)  56232142 
Total length (>= 50000 bp)  29223446 
# contigs                   7828     
Largest contig              239818   
Total length                97946718 
GC (%)                      36.09    
N50                         30630    
N75                         14224    
L50                         867      
L75                         2038     
# N's per 100 kbp           0.00     
